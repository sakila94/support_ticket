<?php

namespace App\Http\Controllers\Agent;

use App\Ticket;
use App\TicketReply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //$replyurl="<a href='{{ url('ticket/".$id."') }}'>clickme </a>";
        if(request()->ajax()) {
            return datatables()->of(Ticket::select('*'))
            ->addColumn('action','<a href="javascript:void(0);" id="ticket-read" data-id="{{ $id }}">View - Reply</a>')
            ->addIndexColumn()
            ->make(true);
        }
        return view('agent.index');
    }
    public function show($id)
    {
        $data['ticket'] = Ticket::find($id);
        $data['reply']=TicketReply::where('ticket_id',$id)->with('agent')->first();
        //dd($data);
        return view('agent.show_ticket', $data);
    }

    public function reply(Request $request)
    {
        $validatedData = $request->validate([
            'reply_msg' => 'required|string',
            'replied_by' => 'required|integer',
            'ticket_id' =>'required|integer'
        ]);
        $newReply = new TicketReply;
        $newReply->reply_msg=$request->reply_msg;
        $newReply->replied_by=$request->replied_by;
        $newReply->ticket_id=$request->ticket_id;
        $newReply->save();

        $updatingTicket = Ticket::find($request->ticket_id);
        $updatingTicket->status=1;
        $updatingTicket->save();

        return redirect('/agent');
    }
}
