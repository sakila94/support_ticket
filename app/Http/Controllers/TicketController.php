<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\TicketReply;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;

class TicketController extends Controller
{
    
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'customer_name' => 'required|string|max:255',
            'problem_description' => 'required',
            'email' =>'required|email',
            'phone' =>'required',
        ]);
        $newTicket = new Ticket;
        $newTicket->customer_name=$request->customer_name;
        $newTicket->problem_description=$request->problem_description;
        $newTicket->email=$request->email;
        $newTicket->phone=$request->phone;
        $newTicket->ref_number= str_random(10);
        $newTicket->save();

        //I comment this mailling function because i can not test with local server and mailling functions is not big deal
        /*
        $maildata = array(
            'name'=>$newTicket->customer_name,
            'ref_number'=>$newTicket->ref_number
        );
     
        Mail::send(['text'=>'mail'], $maildata, function($message) {
           $message->to($newTicket->email, 'Support ticket')
           ->subject('Support Ticket Testing Mail');
           $message->from('backlinks4shakila@gmail.com','sakila jayasooriya');
        });*/


        return redirect('/')->with('message', 'Your Ticket Submitted !');
    }
    public function getTicket(Request $request)
    {
        $ticket = Ticket::where('email',$request->email)->where('ref_number',$request->ref_number)->first();
        $data['ticket']=$ticket;
        if($ticket!=null){
            $reply=TicketReply::where('ticket_id',$ticket->id)->first();
            $data['reply']=$reply;
        }
        else{
            $data['reply']=null;
        }
        return view('public.show-ticket', $data);
    }
}
