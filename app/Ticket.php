<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $fillable = [
        'customer_name','problem_description', 'email', 'phone','status','ref_number'
    ];

}
