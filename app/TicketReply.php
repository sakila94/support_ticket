<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketReply extends Model
{
    protected $table = 'ticket_reply';
    protected $fillable = [
        'reply_msg','replied_by', 'ticket_id'
    ];
    public function agent()
    {
        return $this->hasOne('App\User', 'id', 'replied_by');
    }

    public function ticket()
    {
        return $this->hasOne('App\Ticket', 'ticket_id', 'id');
    }

}
