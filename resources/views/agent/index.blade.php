@extends('layout.app')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-xs-12 colsm-12 col-md-12 col-lg-12 pt-2">
                <h2>Support Tickets</h2>
                <p class="pb-4">Use search box to search tickets by Customer Name,Refference Number or Email</p>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover thead-dark" id="laravel_datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Customer Name</th>
                                <th>Problem Description</th>
                                <th>Status</th>
                                <th>Refference Number</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
        var SITEURL = '{{URL::to('')}}';
        $(document).ready( function () {
          $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                 url: SITEURL + "/agent",
                 type: 'GET',
                },
                columns: [
                         {data: 'id', name: 'id'},
                         { data: 'created_at', name: 'created_at' ,orderable: true,searchable: true},
                         {data: 'customer_name', name: 'customer_name', orderable: false,searchable: true},
                         { data: 'problem_description', name: 'problem_description',orderable: false,searchable: false },
                         { 
                           data: 'status', 
                            render: function(data) { 
                                if(data==0) {
                                    return 'Pending' 
                                }
                                else if (data==1)  {
                                    return 'Replied'
                                }
                            },
                            defaultContent: 'no'
                         },
                         { data: 'ref_number', name: 'ref_number',orderable: true,searchable: true},
                         { data: 'email', name: 'email' ,orderable: true,searchable: true},
                         { data: 'phone', name: 'phone' ,orderable: true,searchable: true},
                         { data: 'action', name: 'action' ,orderable: false,searchable: false}
                      ],
               order: [[0, 'desc']]
             });
    
         
           $('body').on('click', '#ticket-read', function () {
               var ticket_id = $(this).data("id");
               nexturl="/agent/ticket/"+ticket_id;
               window.open(nexturl, '_blank');
           }); 
          
        });
       </script>
@endsection