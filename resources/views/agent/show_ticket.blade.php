@extends('layout.app')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pt-2 pb-2">
                <h2>Ticket : #ID- {{$ticket->id}} (Refference Number : {{$ticket->ref_number}} )</h2>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pt-2">
                <p><b>Ticket ID :</b> {{$ticket->id}} <br>
                    <b>Refference Number :</b> {{$ticket->ref_number}}<br>
                    <b>Created At :</b> {{$ticket->created_at}}
                </p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pt-2">
                <p><b>Customer Name :</b> {{$ticket->customer_name}} <br>
                    <b>Email :</b> {{$ticket->email}}<br>
                    <b>Phone :</b> {{$ticket->phone}}
                </p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pt-2">
                <h1>
                    @if ($ticket->status==0)
                        <b><i style="color:red" class="fas fa-question-circle"></i></b> Pending
                    @elseif($ticket->status==1)
                        <b><i style="color:green" class="fas fa-reply"></i></b> Replied
                    @else
                        <b><i style="color:red" class="fas fa-question-circle"></i></b> Not Defined
                    @endif
                </h1>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pt-2">
                <p>
                    <b>Problem :</b> {{$ticket->problem_description}} 
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pt-2">
                @if ($ticket->status==0)
                    <p>Reply with a good solution for your customer </p>
                    <hr>
                    <form method="POST" action="{{ url('agent/ticket-reply') }}">
                        @csrf
                        <input type="hidden" name="replied_by" value="{{ Auth::user()->id }}" />
                        <input type="hidden" name="ticket_id" value="{{$ticket->id}}" />
                        <div class="form-group">
                            <textarea placeholder="Reply to the ticket" name="reply_msg" id="reply_msg" class="form-control @error('reply_msg') is-invalid @enderror"></textarea>
                            @error('reply_msg')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Send Reply</button>
                        </div>
                    </form>
                @elseif($ticket->status==1 && $reply!=null)
                    <hr>
                    <p>Already Replied to this ticket.<br>
                        <small>Agent:{{ $reply->agent->name }} </small> | 
                        <small>Date:{{$reply->created_at}} </small>
                    </p>
                    <textarea readonly class="form-control">{{$reply->reply_msg}}</textarea>

                @else
                    <p>Something went wrong.<br>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script>
        
</script>
@endsection