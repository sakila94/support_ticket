<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('common.head')
    </head>
    <body>
        <div id="app">
            @include('common.navbar')
            @yield('content')
            @include('common.footer')
        </div>
    </body>
    @yield('scripts')
</html>
