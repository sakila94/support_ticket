@extends('layout.app')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-xs-12 colsm-12 col-md-6 col-lg-6 pt-2">
                <h2 class="pb-4">Submit New Support Ticket</h2>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <form method="POST" action="create">
                    @csrf
                    <div class="form-group">
                        <input placeholder="Your Name" name="customer_name" id="customer_name" type="text" class="form-control @error('customer_name') is-invalid @enderror">
                        @error('customer_name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea placeholder="Problem Description" name="problem_description" id="problem_description" class="form-control @error('problem_description') is-invalid @enderror"></textarea>
                        @error('problem_description')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input placeholder="Email Address" name="email" id="email" type="text" class="form-control @error('email') is-invalid @enderror">
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input placeholder="Phone Number" name="phone" id="phone" type="text" class="form-control @error('phone') is-invalid @enderror">
                        @error('phone')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 colsm-12 col-md-6 col-lg-6 pt-2 bg--secondary">
                <h2 class="pb-4">Search Your Tickets</h2>
                <form method="POST" action="view-ticket">
                        @csrf
                        <div class="form-group">
                            <input placeholder="Email Address" name="email" id="email" type="text" class="form-control @error('email') is-invalid @enderror">
                            @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Refference Number" name="ref_number" id="ref_number" type="text" class="form-control @error('ref_number') is-invalid @enderror">
                            @error('ref_number')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Check Ticket</button>
                        </div>
                    </form>

            </div>
        </div>
    </div>
@endsection