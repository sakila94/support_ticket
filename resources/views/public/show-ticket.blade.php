@extends('layout.app')
@section('content')
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-xs-12 colsm-12 col-md-12 col-lg-12 pt-2 text-center">
                <h2 class="pb-4">Your Support Ticket</h2>
            </div>
            @if ($ticket!=null)
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pt-2">
                    <p><b>Ticket ID :</b> {{$ticket->id}} <br>
                        <b>Refference Number :</b> {{$ticket->ref_number}}<br>
                        <b>Created At :</b> {{$ticket->created_at}}
                    </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pt-2">
                    <p><b>Name :</b> {{$ticket->customer_name}} <br>
                        <b>Email :</b> {{$ticket->email}}<br>
                        <b>Phone :</b> {{$ticket->phone}}
                    </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pt-2">
                    <h1>
                        @if ($ticket->status==0)
                            <b><i style="color:red" class="fas fa-question-circle"></i></b> Pending
                        @elseif($ticket->status==1)
                            <b><i style="color:green" class="fas fa-reply"></i></b> Replied
                        @else
                            <b><i style="color:red" class="fas fa-question-circle"></i></b> Not Defined
                        @endif
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 pt-2">
                    <p>
                        <b>Problem :</b> {{$ticket->problem_description}} 
                    </p>
                </div>
            @else 
                <div class="col-xs-12 colsm-12 col-md-12 col-lg-12 p-5 text-center">
                    <p>Your Entered Email and Refference Number may wrong. Go back and try again </p><br>
                    <a class="btn btn-warning" href={{ url('/') }}> Go Back </a>
                </div>
            @endif
        </div>
        <div class="row">
            @if ($reply!=null)
            <div class="col-xs-12 colsm-12 col-md-12 col-lg-12 p-5 text-center">
                <p>One of Our Agent Replied to this ticket.<br>
                    <small>Agent:{{ $reply->agent->name }} </small> | 
                    <small>Date:{{$reply->created_at}} </small>
                </p>
                <textarea readonly class="form-control">{{$reply->reply_msg}}</textarea>
            </div>
            @endif
        </div>
    </div>
@endsection