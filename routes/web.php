<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('public.index');
});

Route::post('/create','TicketController@create');
Route::post('/view-ticket','TicketController@getTicket');


Route::group(['prefix' => 'agent'], function () {
    Route::get('/','Agent\TicketController@index')->middleware('is_agent');
    Route::get('/ticket/{id}','Agent\TicketController@show')->middleware('is_agent');
    Route::post('/ticket-reply','Agent\TicketController@reply')->middleware('is_agent');
});

Auth::routes();

